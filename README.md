# u8g2-arm-linux-lib

#### 介绍
u8g2 arm linux : https://github.com/wuhanstudio/u8g2-arm-linux

#### 交叉编译工具  

https://releases.linaro.org/components/toolchain/binaries/4.9-2017.01/arm-linux-gnueabihf/  

#### u8g2教程

https://github.com/olikraus/u8g2/wiki  


#### 教程  

1.  项目包含u8g2头文件  
2.  项目调用 libu8g2-arm-linux.a静态库库文件  
3.  开始使用

#### 编译说明 （CMakeList）

1.  添加库  

link_libraries(${PROJECT_SOURCE_DIR}/u8g2/libu8g2-arm-linux.a)

2.  添加头文件

include_directories(${PROJECT_SOURCE_DIR}/u8g2)

3.  设置编译参数

SET(CMAKE_SYSTEM_NAME Linux)  
SET(ARM_PATH /home/watson/arm-gcc/bin)  
SET(CMAKE_C_COMPILER ${ARM_PATH}/arm-linux-gnueabihf-gcc)  
SET(CMAKE_CXX_COMPILER ${ARM_PATH}/arm-linux-gnueabihf-g++)  

SET(CMAKE_CXX_STANDARD 11)  
SET(CMAKE_C_STANDARD 99)  
SET(CMAKE_C_FLAGS "-W -Wall -D __ARM_LINUX__")  
SET(CMAKE_CXX_FLAGS "-W -Wall -D __ARM_LINUX__")  

#### 参与贡献

1.  https://github.com/wuhanstudio/u8g2-arm-linux
2.  https://github.com/olikraus/u8g2
